<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlunosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alunos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alu_nome', 90);
            $table->date('alu_dtnascimento');
            $table->string('alu_logradouro', 120);
            $table->integer('alu_numero');
            $table->string('alu_bairro', 60);
            $table->string('alu_cidade', 40);
            $table->string('alu_cep', 9);
            $table->unsignedInteger('alu_cur_id');
            $table->foreign('alu_cur_id')->references('id')->on('cursos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alunos');
    }
}
