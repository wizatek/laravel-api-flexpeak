<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CursoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cur_nome' => 'required|max:80',
            'cur_pro_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'cur_nome.required' => 'Nome Obrigatório',
            'cur_pro_id.required'  => 'Curso Obrigatório',
        ];
    }
}
