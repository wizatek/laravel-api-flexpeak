<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfessorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pro_nome' => 'required',
            'pro_dtnascimento' => 'required',
        ];
    }

    public function messages()
{
    return [
        'pro_nome.required' => 'Nome Obrigatório',
        'pro_dtnascimento.required'  => 'Data de Nascimento Obrigatória',
    ];
}
}
