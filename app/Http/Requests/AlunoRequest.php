<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlunoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'alu_nome' => 'required|max:90',
            'alu_dtnascimento' => 'required',
            'alu_logradouro' => 'required',
            'alu_numero' => 'required',
            'alu_bairro' => 'required',
            'alu_cidade' => 'required',
            'alu_cep' => 'required',
            'alu_cur_id' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'alu_nome.required' => 'Nome Obrigatório',
            'alu_dtnascimento.required' => 'Data de Nascimento Obrigatória',
            'alu_logradouro.required' => 'Logradouro Obrigatório',
            'alu_numero.required' => 'Número Obrigatório',
            'alu_bairro.required' => 'Bairro Obrigatório',
            'alu_cidade.required' => 'Cidade Obrigatória',
            'alu_cep.required' => 'Cep Obrigatório',
            'alu_cur_id.required' => 'Curso Obrigatório'
        ];
    }
}
