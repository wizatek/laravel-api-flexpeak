<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aluno;
use App\Http\Requests\AlunoRequest;

class AlunoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function index()
    {
        $lista = Aluno::select('alunos.id', 'alunos.alu_nome', 'professors.pro_nome', 'cursos.cur_nome', 'alunos.alu_dtnascimento')
        ->join('cursos', 'cursos.id', '=', 'alunos.alu_cur_id')
        ->join('professors', 'professors.id', '=', 'cursos.cur_pro_id')
        ->get();
        foreach ($lista as $l){
            $l->alu_dtnascimento = date("d/m/Y", strtotime($l->alu_dtnascimento));
        }
        return response()->json($lista);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AlunoRequest $request)
    {
        if(Aluno::create($request->all())){
            $array = ['status' => true];
        }else{
            $array = ['status' => false];
        }
        return response()->json($array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aluno = Aluno::select('alunos.*', 'cur_nome', 'pro_nome')
        ->join('cursos', 'cursos.id', '=', 'alunos.alu_cur_id')
        ->join('professors', 'professors.id', '=', 'cursos.cur_pro_id')
        ->where('alunos.id', $id)
        ->get();

        if($aluno == null){
            $aluno = ["status" => 404];
        }

        return response()->json($aluno);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AlunoRequest $request, $id)
    {
        
        $aluno = Aluno::find($id);

        if($aluno == null){
            $aluno = ["status" => 404];
        }else{
            $aluno->update($request->all());
            $aluno = ["status" => "Alterado com sucesso!"];
        }
        return response()->json($aluno);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $aluno = Aluno::find($id);
        if($aluno == null){
            $aluno = ["status" => 404];
        }else{
            $aluno->delete();
            $aluno = ["status" => "Deletado com sucesso"];
        }
        return response()->json($aluno);
    }
}
