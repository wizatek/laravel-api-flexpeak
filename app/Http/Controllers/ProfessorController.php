<?php

namespace App\Http\Controllers;

use App\Professor;
use Illuminate\Http\Request;
use App\Http\Requests\ProfessorRequest;

class ProfessorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
   
    public function index()
    {
        $lista = Professor::all();

        foreach ($lista as $l){
            $l->pro_dtnascimento = date("d/m/Y", strtotime($l->pro_dtnascimento ));
        }
        return response()->json($lista);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfessorRequest $request)
    {
        $array = Professor::create($request->all());
        return response()->json($array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $professor = Professor::find($id);
        if($professor == null){
            $professor = ["status" => 404];
        }
        return response()->json($professor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfessorRequest $request, $id)
    {
        $professor = Professor::find($id);

        if($professor == null){
            $professor = ["status" => 404];
        }else{
            $professor->update($request->all());
            $professor = ["status" => "Alterado com sucesso"];
        }
        
        return response()->json($professor);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $professor = Professor::find($id);
        if($professor == null){
            $professor = ["status" => 404];
        }else{
            $professor->delete();
            $professor = ["status" => "Deletado com sucesso"];
        }

        return response()->json($professor);
    }
}
