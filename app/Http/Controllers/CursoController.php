<?php

namespace App\Http\Controllers;

use App\Curso;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests\CursoRequest;

class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function index()
    {
        $lista = Curso::with('professor')->get();
        return response()->json($lista);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CursoRequest $request)
    {
        if(Curso::create($request->all())){
            $array = ['status' => true];
        }
        return response()->json($array);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $curso = Curso::find($id);
        if($curso == null){
            $curso = ["status" => 404];
        }
        return response()->json($curso);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CursoRequest $request, $id)
    {
        $curso = Curso::find($id);

        if($curso == null){
            $curso = ["status" => 404];
        }else{
            $curso->update($request->all());
            $curso = ["status" => "Alterado com sucesso"];
        }
        return response()->json($curso);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $curso = Curso::find($id);
        if($curso == null){
            $curso = ["status" => 404];
        }else{
            $curso->delete();
            $curso = ["status" => "Deletado com sucesso"];
        }

        return response()->json($curso);
    }
}
