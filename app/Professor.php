<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    protected $fillable = [
        'pro_nome',
        'pro_dtnascimento'
    ];
}
