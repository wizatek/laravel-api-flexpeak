<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    protected $fillable = [
        'alu_nome',
        'alu_dtnascimento',
        'alu_logradouro',
        'alu_numero',
        'alu_bairro',
        'alu_cidade',
        'alu_cep',
        'alu_cur_id'
    ];
}
