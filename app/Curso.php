<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $fillable = [
        'cur_nome',
        'cur_pro_id'
    ];

    public function professor() {
        return $this->hasOne(Professor::class, 'id', 'cur_pro_id');
    }
}
